﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Website
{
    public partial class projects : System.Web.UI.Page
    {
        private string Email;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Email = Session["Email"].ToString();
            string Uname = Email.Split(new string[] { "@" }, StringSplitOptions.None)[0];
            HELLO.Text = "Hello " + Uname + "!";

            string sqlStatement = "SELECT * FROM PROJECTS INNER JOIN PROJECTUSERS ON PROJECTS.ID = PROJECTUSERS.PROJECTID INNER JOIN USERS ON PROJECTUSERS.USERID = USERS.ID WHERE USERS.EMAILADDRESS = '"+Email+"'";
            //SqlConnection con = new SqlConnection(@"Data Source=(local);Initial Catalog=avi;User ID=root;Password=josh17rog");
            //con.Open();
            //SqlCommand cmd = new SqlCommand(sqlStatement, con);
            //SqlDataReader rdr = cmd.ExecuteReader();
            //ProjGridView.DataSource = rdr;
            //ProjGridView.DataBind();
            //rdr.Close();
            //con.Close();

            try
            {
                MyGlobal.UserSocket.send(sqlStatement);
                string json = MyGlobal.UserSocket.recive();
                var values = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(json);
                List<Dictionary<string, string>> l = values;
                int i = 0;
                foreach (var dict in l)
                {
                    TableRow tr = new TableRow();
                    TableCell ID = new TableCell();
                    TableCell ProjName = new TableCell();
                    TableCell DateCreated = new TableCell();
                    TableCell Link = new TableCell();
                    Button b = new Button();
                    i++;
                    foreach (var keyVal in dict)
                    {
                        ID.Text = i.ToString();
                        if (keyVal.Key == "Name")
                            ProjName.Text = keyVal.Value;
                        if (keyVal.Key == "CreateDate")
                            DateCreated.Text = keyVal.Value;
                    }
                    b.ID = ProjName.Text;
                    b.Text = "Enter";
                    b.Click += (send, EventArgs) => { Enter(sender, EventArgs, b.ID); };
                    Link.Controls.Add(b);
                    tr.Cells.Add(ID);
                    tr.Cells.Add(ProjName);
                    tr.Cells.Add(DateCreated);
                    tr.Cells.Add(Link);
                    ProjTable.Rows.Add(tr);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

        protected void Enter(object sender, EventArgs e, string projName)
        {
            Session["email"] = this.Email;
            Session["project"] = projName;
            Response.Redirect("WebForm2.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("WebForm1.aspx");
        }
    }
}