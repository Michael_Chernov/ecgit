﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Website
{
    public class MySocket
    {
        private Socket ClientSocket;
        
        public MySocket(string host, int port)
        {
            //IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            //IPAddress ipAddr = ipHost.AddressList[0];
            //this.localEndPoint = new IPEndPoint(ipAddr, port);

            //this.ClientSocket = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            //this.ClientSocket.Connect(localEndPoint);
            IPAddress[] IPs = Dns.GetHostAddresses(host);

            this.ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            Console.WriteLine("Establishing Connection to {0}", host);
            this.ClientSocket.Connect(IPs[0], port);
            
        }

        public void close()
        {
            // Close Socket using  
            // the method Close() 
            this.ClientSocket.Shutdown(SocketShutdown.Both);
            this.ClientSocket.Close();
        }

        public bool send(string msg)
        {
            try
            {
                Int32 ml = msg.Length; 
                byte[] msgLen = BitConverter.GetBytes(ml);
                Array.Reverse(msgLen);
                byte[] byMsg = Encoding.UTF8.GetBytes(msg);
                var joint = msgLen.Concat(byMsg);
                byte[] result = joint.ToArray();
                this.ClientSocket.Send(result);
                return true;
            }
            catch (SocketException se)
            {

                Console.WriteLine("SocketException : {0}", se.ToString());
                return false;
            }
        }

        public string recive()
        {
            try
            {
                byte[] bytes = new byte[10000];
                int bytesRec = ClientSocket.Receive(bytes);
                byte[] lenA = new byte[4];
                Array.Copy(bytes, lenA, 4);
                Array.Reverse(lenA);
                int len = BitConverter.ToInt32(lenA, 0);
                byte[] dataA = new byte[len];
                Array.Copy(bytes, 4, dataA, 0, len);
                string msg = Encoding.UTF8.GetString(dataA);
                return msg;
            }
            catch (SocketException se)
            {

                Console.WriteLine("SocketException : {0}", se.ToString());
                return "false";
            }
        }
    }
}