﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Sockets;

namespace Website
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private User u1;
        private bool connected;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                MyGlobal.UserSocket = new MySocket("127.0.0.1", 42069);
                connected = true;
            }
            catch (Exception)
            {
                SIB.Text = "ERROR!";
                SUB.Text = "ERROR!";
                connected = false;
            }
            this.u1 = new User();
        }

        protected void SIB_Click(object sender, EventArgs e)
        {
            if (u1.SignIn(SIE.Text, SIP.Text))
            {
                Session["Email"] = SIE.Text;
                Response.Redirect("projects.aspx");
            }
            else
            {
                if (connected)
                {
                    SIO.Text = "user or password incorrect";
                }
                else
                {
                    SIO.Text = "Can't connect to cloud!";

                    try
                    {
                        MyGlobal.UserSocket = new MySocket("127.0.0.1", 42069);
                    }
                    catch (Exception)
                    {
                        return;
                    }

                    SIB.Text = "SEND";
                    SUB.Text = "SEND";
                }
            }

        }

        protected void SUB_Click(object sender, EventArgs e)
        {
            try
            {
                if (u1.SignUp(SUE.Text, SUP.Text))
                    SUO.Text = "succeed!";
                else
                {
                    if (connected)
                    {
                        SUO.Text = "Email already exist";
                    }
                    else
                    {
                        SUO.Text = "Can't connect to cloud!";
                    }
                }
            }
            catch
            {
                if (connected)
                {
                    SUO.Text = "Email already exist";
                }
                else
                {
                    SUO.Text = "Can't connect to cloud!";
                }

            }
            SUE.Text = "";
            SUP.Text = "";
        }
    }
}