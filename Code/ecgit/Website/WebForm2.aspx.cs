﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Text;


namespace Website
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        private string projName;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.projName = Session["project"].ToString();
            HELLO.Text = projName + " files:";

            string sqlStatement = "SELECT files.Name, files.Data FROM files INNER JOIN updates ON files.UpdateID = updates.ID INNER JOIN projects ON updates.ProjectID = projects.ID AND updates.id = (SELECT MAX(updates.id) FROM updates inner join projects on updates.projectid = projects.id where projects.Name = '" + projName + "');";

            try
            {
                MyGlobal.UserSocket.send(sqlStatement);
                string json = MyGlobal.UserSocket.recive();
                var values = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(json);
                List<Dictionary<string, string>> l = values;
                int i = 0;
                foreach (var dict in l)
                {
                    TableRow tr = new TableRow();
                    TableCell ID = new TableCell();
                    TableCell FileName = new TableCell();
                    TableCell Link = new TableCell();
                    Button b = new Button();
                    string data = "";
                    i++;
                    foreach (var keyVal in dict)
                    {
                        ID.Text = i.ToString();
                        if (keyVal.Key == "Name")
                            FileName.Text = keyVal.Value;
                        if (keyVal.Key == "Data")
                        {
                            data = keyVal.Value;
                            //Decodes base64 data into basic string data, addition by Michael//
                            byte[] rawData = Convert.FromBase64String(data);
                            data = Encoding.UTF8.GetString(rawData);
                        }
                            
                    }
                    b.ID = FileName.Text;
                    b.Text = "Enter";
                    b.Click += (send, EventArgs) => { DataE(sender, EventArgs, data, b.ID); };
                    Link.Controls.Add(b);
                    tr.Cells.Add(ID);
                    tr.Cells.Add(FileName);
                    tr.Cells.Add(Link);
                    ProjTable.Rows.Add(tr);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        protected void DataE(object sender, EventArgs e, string data, string id)
        {
            Session["data"] = data;
            Session["FN"] = id;
            Session["projName"] = this.projName;
            Response.Redirect("dataP.aspx");
        }

        protected void ret_Click(object sender, EventArgs e)
        {
            Session["Email"] = Session["email"].ToString();
            Response.Redirect("projects.aspx");
        }
    }
}