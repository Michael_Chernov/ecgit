﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Website.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ECgit</title>
    <style>h1{font-size: 70px;}
           h2{font-size: 40px;}
           div{font-size: 30px;}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <h1>ECgit</h1>
        <p>&nbsp;</p>    
        <h2>SignIn:</h2>
        <div>
            Email:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="SIE" runat="server"></asp:TextBox>
            <br />
            Password:&nbsp; <asp:TextBox ID="SIP" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="SIB" runat="server" Text="SEND" Height="50px" Width="121px" OnClick="SIB_Click" BorderStyle="Solid" />
            <br />
            <asp:TextBox ID="SIO" runat="server" BorderStyle="None"></asp:TextBox>
        </div>
        <p>&nbsp;</p>    
        <h2>SignUp:</h2>
        <div>
            Email:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="SUE" runat="server"></asp:TextBox>
            <br />
            Password:&nbsp; <asp:TextBox ID="SUP" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="SUB" runat="server" Text="SEND" Height="50px" Width="121px" OnClick="SUB_Click" BorderStyle="Solid" />
            <br />
            <br />
            <asp:TextBox ID="SUO" runat="server" BorderStyle="None"></asp:TextBox>
        </div>
    </form>
</body>
</html>
