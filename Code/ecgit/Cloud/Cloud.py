#!/usr/bin/python 

import pymysql
import socket
import json
import _thread
from enum import Enum
import struct

#global constant vars - db indexes#

PORT = 42069                # Reserve a port for your service.
IP = '127.0.0.1'
DB_PASSWORD = "07032002"
DB_USER = "root"
SCHEMA = "ecgit"
DB_HOST = "localhost"

#PROJECT
PROJECT_ID_INDEX = 2
UPDATE_PROJECT_ID_INDEX = 4
FILE_UPDATE_ID_INDEX = 3
UPDATE_ID_INDEX = 3
FILE_NAME_INDEX = 2
FILE_DATA_INDEX = 1

#USER
USER_ID_INDEX = 2
USER_ID_PASSWORD = 1
USER_ID_EMAIL = 0
USER_ID_DATE_CREATED = 3
CLEARANCE_ID_INDEX = 2
CLEARANCE_ID_DESCRIPTION = 1
CLEARANCE_ID_TITLE = 0

projects = []
updates = []
files = []

users = []
clearance = []

class File:
    def __init__(self, name, data):
        self.name = name
        self.data = data

class Update:
    def __init__(self, date, time, text, files):
        self.date = date
        self.time = time
        self.text = text
        self.files = files

class Project:
    def __init__(self, name, date, updates):
        self.name = name
        self.date = date
        self.updates = updates

    def parseProject():
        main()
class User:
    def __init__(self, email, password, date):
        self.email = email
        self.password = password
        self.date = date

class Clearance:
    def __init__(self, title , desc):
        self.title=title
        self.desc = desc

#Main parsing function
#def parse_db():
#    db = pymysql.connect(
#        host = "localhost",
#        user = "root",
#        password = "josh17rog",
#        db = "ecgit"
#        )
#    project_cursor = db.cursor()
#    project_cursor.execute("SELECT * FROM projects")
#    db.commit()
#    projects_db = project_cursor.fetchall()

#    file_cursor = db.cursor()
#    file_cursor.execute("SELECT * FROM files")
#    files_db = file_cursor.fetchall()

#    update_cursor = db.cursor()
#    update_cursor.execute("SELECT * FROM updates")
#    updates_db = update_cursor.fetchall()

#    #close stuff
#    db.close()
#    project_cursor.close()
#    file_cursor.close()
#    update_cursor.close()

#    for project in projects_db:
#      for update in updates_db:
#          if update[UPDATE_PROJECT_ID_INDEX] == project[PROJECT_ID_INDEX]:
#              for file in files_db:
#                  if file[FILE_UPDATE_ID_INDEX] == update[UPDATE_ID_INDEX]:
#                      files.append(File(file[FILE_NAME_INDEX], file[FILE_DATA_INDEX]))
#              updates.append(Update(update[0], update[1], update[2], files))
#      projects.append(Project(project[1], project[0], updates))

#def usersParse():
#    db = mysql.connector.connect(
#        host = "localhost",
#        user = "root",
#        passwd = "josh17rog",
#        database = "ecgit"
#        )
#    users_cursor = db.cursor()
#    users_cursor.execute("SELECT * FROM users")
#    users_db = project_cursor.fetchall()

#    clearance_cursor = db.cursor()
#    clearance_cursor.execute("SELECT * FROM clearance")
#    clearance_db = file_cursor.fetchall()

#    #close stuff 
#    db.close()
#    users_cursor.close()
#    clearance_cursor.close()

def send_msg(sock, msg):
    # Prefix each message with a 4-byte length (network byte order)
    print("Answer length: " + str(len(msg)))
    msg = len(msg).to_bytes(4, byteorder = 'big') + msg.encode()
    sock.sendall(msg)

def recv_msg(sock):
    # Read message length and unpack it into an integer
    raw_msglen = recvall(sock, 4)
    if not raw_msglen:
        return None
    msglen = struct.unpack('>I', raw_msglen)[0]
    print("Query length: " + str(msglen))
    # Read the message data
    return recvall(sock, msglen)

def recvall(sock, n):
    # Helper function to recv n bytes or return None if EOF is hit
    data = b''
    while len(data) < n:
        packet = sock.recv(n - len(data))
        if not packet:
            return None
        data += packet
    return data

def on_new_client(clientsocket, db):
    project_cursor = db.cursor()

    while True:
        buf = recv_msg(clientsocket)
        cmd = buf.decode()
        if len(cmd) > 0:
            print("Command: " + cmd)
            project_cursor.execute(cmd)
            db.commit()
            projects_db = project_cursor.fetchall()
            
            if projects_db != None:
                response = json.dumps(projects_db)
                print("Client Command succesfully executed!")
                send_msg(clientsocket, response)
            else:
                print("Client Command succesfully executed!")
                send_msg(clientsocket, "")

    #close stuff
    db.close()
    project_cursor.close()

def main():

    db = pymysql.connect(
        host = DB_HOST,
        user = DB_USER,
        password = DB_PASSWORD,
        db = SCHEMA,
        charset='utf8mb4',
        cursorclass=pymysql.cursors.DictCursor
        )
    ####SERVER SOCKET###

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)         # Create a socket object
    host = socket.gethostname() # Get local machine name

    print( 'Server started!')
    print ('Waiting for clients...')

    s.bind((IP, PORT))        # Bind to the port
    s.listen(5)                 # Now wait for client connection.

    print ('Got connection from', host)
    while True:
        c, addr = s.accept()     # Establish connection with client.
        _thread.start_new_thread(on_new_client,(c,db))
    #Note it's (addr,) not (addr) because second parameter is a tuple
    #Edit: (c,addr)
    #that's how you pass arguments to functions when creating new threads using thread module.
    s.close()

    

if __name__ == "__main__":
    main()

