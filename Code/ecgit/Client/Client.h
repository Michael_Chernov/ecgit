#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <string>
#include <map>

#include "Db.h"

enum MainMenu
{
  PROJECT_LIST = 1,
  CHOOSE_PROJECT,
  CLONE_PROJECT,
  CREATE_PROJECT,
  MAIN_EXIT
};

enum ProjectMenu
{
  UPDATE = 1,
  STATUS,
  ADD, 
  REMOVE,
  FILE_LIST,
  LOG,
  PROJECT_EXIT
};

class Client
{
public:
	Client(std::string path);
	~Client();
	void connect(std::string serverIP, int port);
	void mainMenu();

private:

  void printMenu() const;
  void printWelcome() const;
  bool login();
  bool checkLogin(const std::string& email, const std::string& password);
  void printProjects() const;
  void projectMenu(Project& project) const;
  void createProject() const;
  Project* getProject(const int place) const;
  void printProjectMenu(const std::string& projectName) const;
  void cloneProject(Project& project) const;
  void update(Project& project) const;
  void addFile(Project& project) const;
  void removeFile(Project& project) const;
  bool doesLocalRepoExist(const std::string& projectName) const;
  bool doesDirExist(const std::string& dirPath) const;
  void showStatus(std::string projectName) const;
  void printUpdateLog(const std::string& projectName) const;
  void listFiles(const std::string& projectName) const;
  

	SOCKET _clientSocket;
  Db *_db;
  User* user;

  std::string SAVE_PATH;
  
  std::map<std::string, std::vector<File>*> *tempFiles;
};