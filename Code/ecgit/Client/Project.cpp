#include "Project.h"

Project::Project(std::string name, std::string creation_date, std::list<Update>& updates)
{
	this->name_ = name;
	this->creation_date_ = creation_date;
	this->updates_ = updates;
}

Project::Project(std::string name, std::string creation_date)
{
  this->name_ = name;
  this->creation_date_ = creation_date;
  this->updates_ = *(new std::list<Update>());
}

std::string Project::name()
{
	return this->name_;
}

void Project::set_name(std::string name)
{
	this->name_ = name;
}

std::string Project::creation_date()
{
	return this->creation_date_;
}

void Project::set_creation_date(std::string creation_date)
{
	this->creation_date_ = creation_date;
}

std::list<Update>& Project::updates()
{
	return this->updates_;
}

void Project::set_updates(std::list<Update>& updates)
{
	this->updates_ = *(new std::list<Update>(updates));
}
