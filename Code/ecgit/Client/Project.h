#pragma once

#include "Update.h"
#include <string>
#include <list>

class Project
{
public:
	Project(std::string name, std::string creation_date, std::list<Update>& updates);
  Project(std::string name, std::string creation_date);
	
	std::string name();
	void set_name(std::string name);

	std::string creation_date();
	void set_creation_date(std::string creation_date);

	std::list<Update>& updates();
	void set_updates(std::list<Update>& updates);

private:
	std::string name_;
	std::string creation_date_;
	std::list<Update> updates_;
};

