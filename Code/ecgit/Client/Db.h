#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <string>
#include <vector>
#include <map>

#include "User.h"
#include "Project.h"
#include "File.h"
#include "Update.h"
#include "json.hpp"

using json = nlohmann::json;

class Db
{
public:
  Db(SOCKET cloudConnection, std::string path);

  std::vector<User> *getUsers();
  std::vector<Project> *getUserProjects(std::string email);
  void createProject(std::string projectName, std::string email);
  std::vector<File>* getLatestFiles(std::string projectName);
  void update( std::string projectName, std::string msg,std::vector<File>* files);
  std::vector<Update>* getUpdates(std::string projectName);

private:
  void addFile(File& file, const int updateId, const std::string& projectName);

  //Sends data to cloud and serializes the message in a length, data protocol;
  //Length: 4 bytes
  //Data: ? bytes
  // length is the length of the data in ascii bytes.
  void send(std::string& cmd);

  json recv();

  static std::vector<unsigned char> intToBytes(int paramInt);
  static int bytesToInt(std::vector<unsigned char> buffer);

  SOCKET _sock;
  std::string SAVE_PATH;
};

