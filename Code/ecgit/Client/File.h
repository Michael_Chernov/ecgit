#pragma once

#include <string>

class File
{
public:
  File(const std::string& name);
  File(std::string name, std::string data);
  File(const std::string& name, const std::string& data, const bool isAdding);

  std::string name();
  void set_name(std::string name);

  std::string data();
  void set_data(std::string data);

  bool isAdding();

private:
  std::string name_;
  std::string data_;

  //either adding file or removing file.
  bool isAdding_;
};

