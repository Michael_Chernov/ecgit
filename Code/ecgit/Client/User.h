#pragma once

#include <string>

class User
{
public:
	User(std::string email, std::string password);

	std::string email();
	void set_email(std::string email);

	std::string password();
	void set_password(std::string password);

private:
	std::string	email_;
	std::string password_;
};

