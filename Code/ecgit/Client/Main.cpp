#pragma comment (lib, "ws2_32.lib")

#include <WinSock2.h>
#include <Windows.h>
#include "WSAInitializer.h"
#include "Client.h"
#include <exception>
#include <iostream>
#include <fstream>
#include <experimental/filesystem>

using namespace std;

std::string getPath();

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Client c1(getPath());
		c1.connect("127.0.0.1", 42069);
    c1.mainMenu();

	}
	catch (exception& e)
	{
		cout << "Error occured: " << e.what() << endl;
	}

  cout << endl;
	system("PAUSE");
	return 0;
}

std::string getPath()
{
  string path;
  ofstream writeConfig;
  ifstream readConfig;

  readConfig.open("config.txt");
  if (readConfig.is_open())
  {
    readConfig >> path;
    readConfig.close();
    if (path == "")
    {
      do
      {
        cout << "Please provide a path in the configuration file (config.txt)!" << endl;
        system("pause");
        readConfig.open("config.txt");
        if (readConfig.is_open())
        {
          readConfig >> path;
          readConfig.close();
        }
        else
        {
          throw exception("Stop deleting the config file goddamit!");
        }
      } while (path == "");
    }

    if (std::experimental::filesystem::exists(path))
    {
      return path;
    }

    throw exception("Wrong path provided in configuration file!");
  }

  writeConfig.open("config.txt");
  writeConfig.close();
  cout << "Welcome user!" << endl << "Please provide a full path in the config.txt file "<< endl << "for the location of the desired local repository on this PC." << endl;
  system("pause");

  readConfig.open("config.txt");
  if (readConfig.is_open())
  {
    readConfig >> path;
    readConfig.close();
    if (path == "")
    {
      do
      {
        cout << "Please provide a path in the configuration file (config.txt)!" << endl;
        system("pause");
        readConfig.open("config.txt");
        if (readConfig.is_open())
        {
          readConfig >> path;
          readConfig.close();
        }
        else
        {
          throw exception("Stop deleting the config file goddamit!");
        }
      } while (path == "");
    }

    if (std::experimental::filesystem::exists(path))
    {
      return path;
    }

    throw exception("Wrong path provided in configuration file!");
  }

  throw exception("Stop deleting the config file goddamit!");
}