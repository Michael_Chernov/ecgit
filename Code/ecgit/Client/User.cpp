#include "User.h"

User::User(std::string email, std::string password)
{
	this->email_ = email;
	this->password_ = password;
}

std::string User::email()
{
	return this->email_;
}

void User::set_email(std::string email)
{
	this->email_ = email;
}

std::string User::password()
{
	return this->password_;
}

void User::set_password(std::string password)
{
	this->password_ = password;
}
