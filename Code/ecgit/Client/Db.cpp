#define _CRT_SECURE_NO_WARNINGS

#include "Db.h"

#include "base64.h"
#include <iostream>

#include <iomanip>
#include <ctime>
#include <sstream>
#include <fstream>

using namespace std;

Db::Db(SOCKET cloudConnection, std::string path)
{
  this->_sock = cloudConnection;
  this->SAVE_PATH = path;
}

vector<User> *Db::getUsers()
{
  string query = "SELECT * FROM USERS;";

  this->send(query);

  json results = Db::recv();


  vector<User> *users = new vector<User>();

  for (unsigned int i = 0; i < results.size(); i++)
  {
    users->push_back(*(new User(results[i]["EmailAddress"].get<string>(), results[i]["Password"].get<string>())) );
  }

  return users;
}

vector<Project> *Db::getUserProjects(string email)
{
  string query = "SELECT * FROM PROJECTS INNER JOIN PROJECTUSERS ON PROJECTS.ID = PROJECTUSERS.PROJECTID INNER JOIN USERS ON PROJECTUSERS.USERID = USERS.ID WHERE USERS.EMAILADDRESS = \"" + email + "\";";

  this->send(query);

  
  json results = Db::recv();

  vector<Project>* projects = new vector<Project>();

  for (unsigned int i = 0; i < results.size(); i++)
  {
    projects->push_back(*(new Project(results[i]["Name"].get<string>(), results[i]["CreateDate"].get<string>())));
  }

  return projects;
}

void Db::createProject(string projectName, string email)
{
  auto t = time(nullptr);
  auto tm = *localtime(&t);

  ostringstream oss;
  oss << put_time(&tm, "%d.%m.%Y");
  auto timeStr = oss.str();

  string query1 = "INSERT INTO projects (CreateDate, Name) VALUES ('" + timeStr + "', '" + projectName + "');";
  string query2 = "INSERT INTO PROJECTUSERS (DateJoined, UserID, ProjectID) VALUES ('" + timeStr + "', (SELECT ID FROM USERS WHERE EmailAddress = '" + email + "') , (SELECT ID FROM projects WHERE name = '" + projectName + "' ) );";

  this->send(query1);

  Db::recv();

  this->send(query2);

  Db::recv();
}

vector<File>* Db::getLatestFiles(string projectName)
{
  string query = "SELECT files.Name, files.Data FROM files INNER JOIN updates ON files.UpdateID = updates.ID INNER JOIN projects ON updates.ProjectID = projects.ID AND updates.id = (SELECT MAX(updates.id) FROM updates inner join projects on updates.projectid = projects.id where projects.Name = '" + projectName + "');";

  this->send(query);

  json results = Db::recv();

  vector<File>* files = new vector<File>();

  for (unsigned int i = 0; i < results.size(); i++)
  {
    //Decode base64 data into basic string.
    string encodedData = results[i]["Data"].get<string>();

    vector<BYTE> decodedData = base64_decode(encodedData);

    string *decodedStr = new string(decodedData.begin(), decodedData.end());

    files->push_back(*(new File(results[i]["Name"].get<string>(), *decodedStr)));
  }

  return files;
}

void Db::update(std::string projectName, std::string msg, std::vector<File>* files)
{
  vector<File>* currFiles = getLatestFiles(projectName);

  auto t = time(nullptr);
  auto tm = *localtime(&t);
  ostringstream oss1;
  ostringstream oss2;
  oss1 << put_time(&tm, "%d.%m.%Y");
  auto date = oss1.str();
  oss2 << put_time(&tm, "%H:%M:%S");
  auto time = oss2.str();

  string selectQuery = "SELECT MAX(id) FROM updates";

  this->send(selectQuery);

  json results = Db::recv();

  int updateId = results[0]["MAX(id)"] + 1;

  ostringstream insertQueryStream;
  insertQueryStream << "INSERT INTO updates (Text, Date, Time, ProjectID, ID) VALUES ('";
  insertQueryStream << msg << "', '" << date << "', '" << time << "', ";
  insertQueryStream << "(SELECT id FROM projects WHERE Name = '" << projectName << "'), ";
  insertQueryStream << to_string(updateId) << " );";
  string insertQuery = insertQueryStream.str();

  this->send(insertQuery);

  Db::recv();


  //update currFiles//
  vector<File>::iterator it = currFiles->begin();

  for (; it != currFiles->end(); it++)
  {
    std::string contents;
    std::string path;

    path = SAVE_PATH;

    path += "/" + projectName + "/" + it->name();

    ifstream fileStream(path, ios::binary | ios::ate);
    streamsize size = fileStream.tellg();
    fileStream.seekg(0, std::ios::beg);

    std::vector<char> buffer(size);
    if (fileStream.read(buffer.data(), size))
    {
      contents = std::string(buffer.begin(), buffer.end());
      /* worked! */
      fileStream.close();
    }

    it->set_data(contents);
  }

  if (files != nullptr)
  {
    for (File file : *files)
    {
      if (file.isAdding())
      {
        currFiles->push_back(file);
      }
      else
      {
        vector<File>::iterator it = currFiles->begin();
        for (; it != currFiles->end(); it++)
        {
          if (it->name() == file.name())
          {
            currFiles->erase(it);
            it = currFiles->begin();
          }
        }
      }
    }
  }
  

  for (File file : *currFiles)
  {
    addFile(file, updateId, projectName);
  }

  delete currFiles;
  if (files != nullptr)
  {
    delete files;
  }
}

vector<Update>* Db::getUpdates(std::string projectName)
{
  string query = "SELECT updates.text, updates.date, updates.time FROM updates INNER JOIN projects ON updates.projectID = projects.ID where projects.Name = '" + projectName + "';";

  this->send(query);

  json results = Db::recv();

  vector<Update>* updates = new vector<Update>();

  for (unsigned int i = 0; i < results.size(); i++)
  {
    updates->push_back(*(new Update(results[i]["text"].get<string>(), results[i]["date"].get<string>(), results[i]["time"].get<string>())));
  }

  return updates;
}

void Db::send(std::string& cmd)
{
  string editedCmd = cmd;
  vector<unsigned char> cmdLenVec = intToBytes(cmd.size());
  string cmdLen(cmdLenVec.begin(), cmdLenVec.end());
  editedCmd.insert(0, cmdLen);

  ::send(this->_sock, editedCmd.c_str(), editedCmd.size(), 0);
}

json Db::recv()
{
  char rawLen[4];

  ::recv(this->_sock, rawLen, 4, 0); //get length of message

  int i = 0;
  vector<unsigned char> lenVec(4);
  vector<unsigned char>::iterator it = lenVec.begin();

  for (;it != lenVec.end(); it++)
  {
    *it = rawLen[i];
    ++i;
  }

  int len = bytesToInt(lenVec);

  if (len == 0)
  {
    return nullptr;
  }
  
  char* rawResult = new char[len + 1]();
  char* single = new char[1]();

  for (int i = 0; i < len; i++)
  {
    ::recv(this->_sock, single, 1, 0);
    rawResult[i] = single[0];
  }
  
  json result = json::parse(rawResult);

  delete[] rawResult;

  return result;
}

void Db::addFile(File& file, const int updateId, const string& projectName)
{
  ostringstream queryStream;

  string fileData = file.data();

  //encode basic string into base64 data.
  vector<BYTE> data(fileData.begin(), fileData.end());
  std::string encodedData = base64_encode(&data[0], data.size());

  if (!file.isAdding())
  {
    ostringstream selectQueryStream;

    selectQueryStream << "select max(files.id) from files inner join updates on updates.id = files.UpdateID inner join projects on updates.ProjectID = projects.id";
    selectQueryStream << " where files.name = '" << file.name() << "' and projects.name = '" << projectName << "';";

    string selectQuery = selectQueryStream.str();

    this->send(selectQuery);

    json results = Db::recv();
    int previousId = results[0]["max(files.id)"];
    
    queryStream << "INSERT INTO files (Name, Data, UpdateID, PreviousID) VALUES ('";
    queryStream << file.name() << "', \"" << encodedData << "\", ";
    queryStream << to_string(updateId) << " , ";
    queryStream << previousId << ");";
  }
  else
  {
    queryStream << "INSERT INTO files (Name, Data, UpdateID) VALUES ('";
    queryStream << file.name() << "', \"" << encodedData << "\", ";
    queryStream << to_string(updateId) << ");";
  }

  string query = queryStream.str();

  this->send(query);
  
  Db::recv();
}

vector<unsigned char> Db::intToBytes(int paramInt)
{
  vector<unsigned char> arrayOfByte(4);
  for (int i = 0; i < 4; i++)
  {
    arrayOfByte[3 - i] = (paramInt >> (i * 8));
  }
  return arrayOfByte;
}

int Db::bytesToInt(vector<unsigned char> buffer)
{
  int a = int((unsigned char)(buffer[0]) << 24 |
    (unsigned char)(buffer[1]) << 16 |
    (unsigned char)(buffer[2]) << 8 |
    (unsigned char)(buffer[3]));
  return a;
}
