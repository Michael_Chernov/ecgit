#include "File.h"

File::File(const std::string& name)
{
  this->name_ = name;
  this->data_ = "";
  this->isAdding_ = false;
}

File::File(std::string name, std::string data)
{
  this->name_ = name;
  this->data_ = data;
  this->isAdding_ = false;
}

File::File(const std::string& name, const std::string& data, const bool isAdding)
{
  this->name_ = name;
  this->data_ = data;
  this->isAdding_ = isAdding;
}

std::string File::name()
{
  return this->name_;
}

void File::set_name(std::string name)
{
  this->name_ = name;
}

std::string File::data()
{
  return this->data_;
}

void File::set_data(std::string data)
{
  this->data_ = data;
}

bool File::isAdding()
{
  return this->isAdding_;
}
