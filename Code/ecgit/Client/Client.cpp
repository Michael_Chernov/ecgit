﻿#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "Client.h"
#include "User.h"

#include <algorithm>
#include <exception>
#include <string>
#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <experimental/filesystem>

using namespace std;

Client::Client(std::string path)
{
	// we connect to server that uses TCP. thats why SOCK_STREAM & IPPROTO_TCP
	_clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_clientSocket == INVALID_SOCKET)
		throw exception(__FUNCTION__ " - socket");
  
  _db = new Db(_clientSocket, path);

  this->user = nullptr;
  this->tempFiles = new map<string, vector<File>*>();
  this->SAVE_PATH = path;
}

Client::~Client()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_clientSocket);
	}
	catch (...) {}

  delete _db;

  if (user != nullptr)
  {
    delete user;
  }

  delete tempFiles;
}

void Client::connect(string serverIP, int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen to
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr =  inet_addr(serverIP.c_str());    // the IP of the server

	// notice that we step out to the global namespace
	// for the resolution of the function socket
	int status = ::connect(_clientSocket, (struct sockaddr*)&sa, sizeof(sa));

	if (status == INVALID_SOCKET)
		throw exception("Can't connect to server");
}

void Client::mainMenu()
{

  printWelcome();
  while (!login())
  {
    cout << endl << "ERROR! Wrong username or password!" << endl;
    cout << "Try again";
    Sleep(800);
    cout << ".";
    Sleep(800);
    cout << ".";
    Sleep(800);
    cout << "." << endl;
    Sleep(800);

    system("cls");
  }
  
  string input = "";
  string projectNum = "";

  while (input != "5")
  {
    printMenu();
    cout << endl;
    cout << "Choose an option: ";
    cin >> input;
    cout << endl;
    int inputNum = 0;
    int choice = 0;
    Project* projectPtr = nullptr;

    try
    {
      inputNum = atoi(input.c_str());
    }
    catch (const exception&)
    {
      inputNum = 0;
    }

    switch (inputNum)
    {
    case MainMenu::PROJECT_LIST:
      printProjects();
      cout << endl;
      system("pause");
      break;

    case MainMenu::CHOOSE_PROJECT:
      do
      {
        system("cls");
        printProjects();
        cout << endl << "Choose a project: ";
        cin >> projectNum;

        try
        {
          choice = atoi(projectNum.c_str());
        }
        catch (const exception&)
        {
          choice = 0;
        }

        projectPtr = getProject(choice);

        if (projectPtr == nullptr)
        {
          cout << endl <<"ERROR! Wrong input!" << endl;
          system("pause");
        }

      } while (projectPtr == nullptr);

      system("cls");

      if (doesLocalRepoExist(projectPtr->name()))
      {
        cout << "Existing project local repository found!" << endl <<endl;
        cout << "Project local repository is located in:" << endl;
        cout << SAVE_PATH << "/" << projectPtr->name() << endl << endl;
      }
      else
      {
        cout << "Project local repository not found!" << endl << endl;
        cout << "Cloning project into a local repository";
        Sleep(800);
        cout << ".";
        Sleep(800);
        cout << ".";
        Sleep(800);
        cout << "." << endl << endl;

        cloneProject(*projectPtr);

        cout << "Local repository is located in:" << endl;
        cout << SAVE_PATH << "/" << projectPtr->name() << endl << endl;
      }
      
      system("pause");

      projectMenu(*projectPtr);

      delete projectPtr;

      break;
      
    case MainMenu::CLONE_PROJECT:

      do
      {
        system("cls");
        printProjects();
        cout << endl << "Choose a project: ";
        cin >> projectNum;

        try
        {
          choice = atoi(projectNum.c_str());
        }
        catch (const exception&)
        {
          choice = 0;
        }

        projectPtr = getProject(choice);

        if (projectPtr == nullptr)
        {
          cout << endl << "ERROR! Wrong input!" << endl;
          system("pause");
        }

      } while (projectPtr == nullptr);

      system("cls");

      cout << "Cloning project into a local repository";
      Sleep(800);
      cout << ".";
      Sleep(800);
      cout << ".";
      Sleep(800);
      cout << "." << endl << endl;

      cloneProject(*projectPtr);

      cout << "Local repository is located in:" << endl;
      cout << SAVE_PATH << "/" << projectPtr->name() << endl << endl;

      delete projectPtr;

      system("pause");
      break;
      
    case MainMenu::CREATE_PROJECT:
      createProject();
      break;

    case MainMenu::MAIN_EXIT:
      break;

    default:
      cout << "Error! Wrong input!" << endl;
      system("pause");
      break;
    }

    system("cls");
  }

  cout << "Goodbye " << this->user->email() << "!" << endl;
}

void Client::printMenu() const
{
  cout << "Main menu:" << endl;
  cout << "1. Print Projects" << endl;
  cout << "2. Choose Project" << endl;
  cout << "3. Clone Project" << endl;
  cout << "4. Create Project" << endl;
  cout << "5. Exit" << endl;
}

void Client::printWelcome() const
{
  cout << "Welcome to";
  Sleep(800);
  cout << ".";
  Sleep(800);
  cout << ".";
  Sleep(800);
  cout << "." << endl << endl;
  cout << R"~(__/\\\\\\\\\\\\\\\________/\\\\\\\\\_____/\\\\\\\\\\\\_____________________        )~" << endl;
  Sleep(100);
  cout << R"~( _\/\\\///////////______/\\\////////____/\\\//////////______________________       )~" << endl;
  Sleep(100);
  cout << R"~(  _\/\\\_______________/\\\/____________/\\\______________/\\\_____/\\\______      )~" << endl;
  Sleep(100);
  cout << R"~(   _\/\\\\\\\\\\\______/\\\_____________\/\\\____/\\\\\\\_\///___/\\\\\\\\\\\_     )~" << endl;
  Sleep(100);
  cout << R"~(    _\/\\\///////______\/\\\_____________\/\\\___\/////\\\__/\\\_\////\\\////__    )~" << endl;
  Sleep(100);
  cout << R"~(     _\/\\\_____________\//\\\____________\/\\\_______\/\\\_\/\\\____\/\\\______   )~" << endl;
  Sleep(100);
  cout << R"~(      _\/\\\______________\///\\\__________\/\\\_______\/\\\_\/\\\____\/\\\_/\\__  )~" << endl;
  Sleep(100);
  cout << R"~(       _\/\\\\\\\\\\\\\\\____\////\\\\\\\\\_\//\\\\\\\\\\\\/__\/\\\____\//\\\\\___ )~" << endl;
  Sleep(100);
  cout << R"~(        _\///////////////________\/////////___\////////////____\///______\/////____)~" << endl << endl << endl << endl;
}

bool Client::login()
{
  string email, password;

  cout << R"(///Login:////)" << endl;
  cout << "Email: ";

  cin >> email;

  cout << "Password: ";
  cin >> password;

  return checkLogin(email, password);
}

bool Client::checkLogin(const string& email, const string& password)
{
  vector<User> *users = this->_db->getUsers();

  for (User& user : *users)
  {
    if (user.email() == email && user.password() == password)
    {
      system("cls");
      cout << "Welcome back " << user.email() << "!" << endl << endl;

      this->user = new User(user);

      delete users;

      return true;
    }
  }

  delete users;

  return false;
}

void Client::printProjects() const
{
  int i = 1;
  vector<Project> *projects = _db->getUserProjects(this->user->email());

  while (!projects->empty())
  {
    cout << to_string(i) << ". " << projects->back().name() << endl;
    projects->pop_back();
    ++i;
  }

  delete projects;
}

Project* Client::getProject(const int place) const
{
  vector<Project>* projects = _db->getUserProjects(this->user->email());

  if (place <= (int)(_db->getUserProjects(this->user->email())->size()) && place > 0)
  {
    Project* proj = new Project((*projects)[projects->size() - place]);

    delete projects;
    return proj;
  }

  delete projects;

  return nullptr;
}

void Client::projectMenu(Project& project) const
{
  system("cls");

  string input = "";

  while (input != "7")
  {
    printProjectMenu(project.name());
    cout << "Choose an option: ";
    cin >> input;
    cout << endl;
    int inputNum = 0;
    int choice = 0;

    try
    {
      inputNum = atoi(input.c_str());
    }
    catch (const exception&)
    {
      inputNum = 0;
    }

    switch (inputNum)
    {
    case ProjectMenu::UPDATE:
      update(project);
      break;

    case ProjectMenu::ADD:
      try
      {
        addFile(project);
      }
      catch (const std::exception& e)
      {
        cout << "ERROR! " << e.what() << endl << endl;
        system("pause");
      }
      break;

    case ProjectMenu::REMOVE:
      try
      {
        removeFile(project);
      }
      catch (const std::exception & e)
      {
        cout << "ERROR! " << e.what() << endl << endl;
        system("pause");
      }
      break;

    case ProjectMenu::STATUS:
      showStatus(project.name());
      break;

    case ProjectMenu::LOG:
      printUpdateLog(project.name());
      system("pause");
      break;

    case ProjectMenu::FILE_LIST:
      listFiles(project.name());
      system("pause");
      break;

    case ProjectMenu::PROJECT_EXIT:
      break;

    default:
      cout << "Error! Wrong input!" << endl;
      system("pause");
      break;
    }

    system("cls");
  }

}

void Client::createProject() const
{
  string input = "";

  system("cls");
  cout << "New project name: ";
  cin >> input;

  _db->createProject(input, user->email());

  cout << endl << "Project " << input << " successfully created!" << endl << endl;

  cout << "Project local repository is being created";
  Sleep(800);
  cout << ".";
  Sleep(800);
  cout << ".";
  Sleep(800);
  cout << "." << endl << endl;

  string path = SAVE_PATH;
  path += "/" + input;

  //create directory for project (Local repo)
  std::experimental::filesystem::create_directory(path);

  cout << "Local repository is located in:" << endl;
  cout << path << endl << endl;

  system("pause");
}

void Client::printProjectMenu(const std::string& projectName) const
{
  cout << "Project \"" + projectName + "\" menu:" << endl;
  cout << "1. Update Project" << endl;
  cout << "2. Show project files status" << endl;
  cout << "3. Add file" << endl;
  cout << "4. Remove file" << endl;
  cout << "5. Files list" << endl;
  cout << "6. Update log" << endl;
  cout << "7. Back to main menu" << endl << endl;
}

void Client::cloneProject(Project& project) const
{
  vector<File>* files = _db->getLatestFiles(project.name());
  string path = SAVE_PATH;
  path += "/" + project.name();
  
  //create directory for project (Local repo)
  std::experimental::filesystem::create_directory(path);

  path += "/";

  if (files->empty())
  {
    cout << "Warning! Cloned an empty project!" << endl;
    delete files;
    return;
  }

  for (File& file : *files)
  {
    ofstream fileStream;
    fileStream.open(path + file.name());
    if (fileStream.is_open())
    {
      fileStream << file.data();
      fileStream.close();
    }
    else
    {
      throw exception("Unable to create file!");
    }
  }

  delete files;
}

void Client::update(Project& project) const
{
  std::string msg;

  system("cls");
  cout << "Please enter update message: " << endl;
  cin >> msg;

  _db->update(project.name(), msg, (*tempFiles)[project.name()]);

  cout << endl << "Updating project " << project.name();
  Sleep(800);
  cout << ".";
  Sleep(800);
  cout << ".";
  Sleep(800);
  cout << "." << endl << endl;;

  cout << "Project " << project.name() << " was successfuly updated!" << endl << endl;

  system("pause");
}

void Client::addFile(Project& project) const
{
  std::string fileName;
  std::string contents;
  std::string path;

  cout << "Enter File name to add: ";

  cin >> fileName;

  path = SAVE_PATH;

  path += "/" + project.name() + "/" + fileName;

  ifstream fileStream(path, ios::binary | ios::ate);
  if (fileStream.is_open())
  {
    streamsize size = fileStream.tellg();
    fileStream.seekg(0, std::ios::beg);

    std::vector<char> buffer(size);

    if (fileStream.read(buffer.data(), size))
    {
      contents = std::string(buffer.begin(), buffer.end());
      /* worked! */
    }
    else
    {
      throw exception("Can't read file/ file doesn't exist!");
    }

    fileStream.close();
  }
  else
  {
    throw exception("Can't read file/ file doesn't exist!");
  }

  vector<File>* files = _db->getLatestFiles(project.name());
  
  for (File file : *files)
  {
    if (fileName == file.name())
    {
      delete files;
      throw exception("File already exists in project!");
    }
  }

  delete files;

  File* file = new File(fileName, contents, true);

  if ((*tempFiles)[project.name()] == nullptr)
  {
    (*tempFiles)[project.name()] = new vector<File>();
  }

  for (File file : *((*tempFiles)[project.name()]))
  {
    if (fileName == file.name())
    {
      throw exception("File already already added to list!");
    }
  }

  (*tempFiles)[project.name()]->push_back(*file);

  cout << "Adding file " << fileName;
  Sleep(800);
  cout << ".";
  Sleep(800);
  cout << ".";
  Sleep(800);
  cout << "." << endl << endl;

  cout << "File " << fileName << " was successfuly added to pending list!" << endl << endl;

  system("pause");
}

void Client::removeFile(Project& project) const
{
  File *foundFile = nullptr;
  std::string fileName;

  cout << "Enter File name to remove: ";

  cin >> fileName;

  vector<File>* files = _db->getLatestFiles(project.name());

  for (File file : *files)
  {
    if (fileName == file.name())
    {
      foundFile = new File(file.name());
    }
  }
  if (foundFile == nullptr)
  {
    delete files;
    throw exception("File doesn't exist in project!");
  }

  delete files;

  if ((*tempFiles)[project.name()] == nullptr)
  {
    (*tempFiles)[project.name()] = new vector<File>();
  }

  for (File file : *((*tempFiles)[project.name()]))
  {
    if (fileName == file.name())
    {
      throw exception("File already already added to list!");
    }
  }

  (*tempFiles)[project.name()]->push_back(*foundFile);

  cout << "Removing file " << fileName;
  Sleep(800);
  cout << ".";
  Sleep(800);
  cout << ".";
  Sleep(800);
  cout << "." << endl << endl;

  cout << "File " << fileName << " was successfuly added to remove pending list!" << endl << endl;

  system("pause");
}

bool Client::doesLocalRepoExist(const std::string& projectName) const
{
  std::string path = SAVE_PATH;
  path += "/" + projectName;
  return doesDirExist(path);
}

bool Client::doesDirExist(const std::string& dirPath) const
{
  int ftyp = GetFileAttributesA(dirPath.c_str());
  if (ftyp == INVALID_FILE_ATTRIBUTES)
    return false;  //something is wrong with your path!

  if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
    return true;   // this is a directory!

  return false;
}

void Client::showStatus(string projectName) const
{
  if ((*tempFiles)[projectName] == nullptr || (*tempFiles)[projectName]->empty())
  {
    cout << "Nothing to update." << endl << endl;
    system("pause");
    return;
  }
  unsigned int i = 1;
  cout << "Files to be added: " << endl;

  for (File file: *((*tempFiles)[projectName]))
  {
    if (file.isAdding())
    {
      cout << to_string(i) << ". " << file.name() << endl;
      ++i;
    }
  }

  i = 1;
  cout << endl << "Files to be removed: " << endl;

  for (File file : *((*tempFiles)[projectName]))
  {

    if (!file.isAdding())
    {
      cout << to_string(i) << ". " << file.name() << endl;
      ++i;
    }
  }

  cout << endl;
  system("pause");
}

void Client::printUpdateLog(const std::string& projectName) const
{
  int i = 1;

  cout << "Project '" << projectName << "' update log:" << endl << endl;

  vector<Update> *updates = _db->getUpdates(projectName);

  for (Update update : *updates)
  {
    cout << i << ". '" << update.text() << "'\t\t\t" << update.date() << "\t\t\t" << update.time() << endl;
    ++i;
  }

  delete updates;
}

void Client::listFiles(const std::string& projectName) const
{
  int i = 1;

  std::vector<File> *files =  _db->getLatestFiles(projectName);

  cout << "Project '" << projectName << "' file list:" << endl << endl;

  for (File file : *files)
  {
    cout << i << ". '" << file.name() << "'" << endl;
    ++i;
  }

  cout << endl;

  delete files;

}
