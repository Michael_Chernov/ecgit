#pragma once

#include "File.h"
#include <string>
#include <list>

class Update
{
public:
  Update(std::string text, std::string date, std::string time, std::list<File>& files);
  Update(std::string text, std::string date, std::string time);

  std::string text();
  void set_text(std::string text);

  std::string date();
  void set_date(std::string date);

  std::string time();
  void set_time(std::string time);

  std::list<File>& files();
  void set_files(std::list<File>& files);


private:
  std::string text_;
  std::string date_;
  std::string time_;
  std::list<File> files_;

};

