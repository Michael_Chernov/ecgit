#pragma once

#include <string>
class Clearance
{
public:
	Clearance(std::string title, std::string description);

	std::string title();
	void set_title(std::string title);

	std::string description();
	void set_description(std::string description);

private:
	std::string title_;
	std::string description_;

};

