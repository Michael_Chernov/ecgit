#include "Update.h"

Update::Update(std::string text, std::string date, std::string time, std::list<File>& files)
{
  this->text_ = text;
  this->date_ = date;
  this->time_ = time;
  this->files_ = files;
}

Update::Update(std::string text, std::string date, std::string time)
{
  this->text_ = text;
  this->date_ = date;
  this->time_ = time;

  this->files_ = *(new std::list<File>());
}

std::string Update::text()
{
	return this->text_;
}

void Update::set_text(std::string text)
{
	this->text_ = text;
}

std::string Update::date()
{
	return this->date_;
}

void Update::set_date(std::string date)
{
	this->date_ = date;
}

std::string Update::time()
{
	return this->time_;
}

void Update::set_time(std::string time)
{
	this->time_ = time;
}

std::list<File>& Update::files()
{
	return this->files_;
}

void Update::set_files(std::list<File>& files)
{
	this->files_ = *(new std::list<File>(files));
}
