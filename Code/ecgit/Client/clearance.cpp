#include "Clearance.h"

Clearance::Clearance(std::string title, std::string description)
{
	this->title_ = title;
	this->description_ = description;
}

std::string Clearance::title()
{
	return this->title_;
}

void Clearance::set_title(std::string title)
{
	this->title_ = title;
}

std::string Clearance::description()
{
	return this->description_;
}

void Clearance::set_description(std::string description)
{
	this->description_ = description;
}
